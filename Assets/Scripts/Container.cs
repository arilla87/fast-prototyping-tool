using System.Collections.Generic;
using System;

[Serializable]
public class Container
{
    public List<ObjectsData> objectsList = new List<ObjectsData>();
}