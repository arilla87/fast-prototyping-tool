﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class ListToDropDownAttribute : PropertyAttribute
{
    public Type myTipe;
    public string propertyName;
    public ListToDropDownAttribute(Type _mType, string _propertyName)
    {
        myTipe = _mType;
        propertyName = _propertyName;
    }
}

[CustomPropertyDrawer(typeof(ListToDropDownAttribute))]
public class ListToDropDownDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) 
    {
        ListToDropDownAttribute atb = attribute as ListToDropDownAttribute;
        List<string> stringList = null;
        if(atb.myTipe.GetField(atb.propertyName) != null)
        {
            stringList = atb.myTipe.GetField(atb.propertyName).GetValue(atb.myTipe) as List<string>;
        }  
        if(stringList != null && stringList.Count > 0)
        {
            int selectedIndex = Math.Max(stringList.IndexOf(property.stringValue), 0);

            selectedIndex = EditorGUI.Popup(position, property.name, selectedIndex, stringList.ToArray());
            property.stringValue = stringList[selectedIndex];
        }
        else
        {
            EditorGUI.PropertyField(position, property, label);
        }
    }
}
public class DropDownMenu : MonoBehaviour
{
    public List<GameObject> goListToSpawn;

    public static List<string> goToSelect;
    [ListToDropDownAttribute(typeof(DropDownMenu), "goToSelect")]
    public string goSelected;

    void Start()
    {
        CreateGoList();        
    }
    public void CreateGoList()
    {
        List<string> tmpList = new List<string>();

        for(int i = 0 ; i < goListToSpawn.Count ; i++)
        {
            tmpList.Add(goListToSpawn[i].name);

        }
        goToSelect = tmpList;
        goSelected = goToSelect[0];
    }

}
