﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Json
{
	public class JsonHelper
	{
		public static string ToJson<T>(T[] array)
		{
			Wrapper<T> wrapper = new Wrapper<T>();
			wrapper.array = array;
			return UnityEngine.JsonUtility.ToJson(wrapper);
		}

		public static T[] getJsonArray<T>(string json)
		{
			string newJson = "{ \"array\": " + json + "}";
			Debug.Log(newJson);
			Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>> (json);
			return wrapper.array;
		}

		[Serializable]
		private class Wrapper<T>
		{
			public T[] array;
		}

		public static string LoadJsonResource(string path) {
			string filePath = "json/" + path.Replace (".json", "");
			TextAsset targetFile = Resources.Load<TextAsset> (filePath);
			return targetFile.text;
		}
	}
}