﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using Json;
using System.IO;
public class ObjectSpawner : MonoBehaviour
{
    public Camera camera;
    public GameObject projectil;
    public GameObject particles;
    public Transform projectilSpawner;
    private DropDownMenu dropMenu;
    private Container c;
    // Start is called before the first frame update
    void Start()
    {
        dropMenu = GameObject.Find("ObjectMenu").GetComponent<DropDownMenu>();
        c = new Container();
        
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit)) 
            {
                Transform objectHit = hit.transform;
                GameObject p = Instantiate(projectil, projectilSpawner.transform.position, Quaternion.identity);
                p.transform.Translate(hit.transform.position);
                p.GetComponent<Rigidbody>().AddForce(Vector3.forward * 200);
                // p.GetComponent<Rigidbody>().AddForce(p.transform.forward *Time.deltaTime * 20);
                GameObject selectedGo = dropMenu.goListToSpawn.Where(obj => obj.name == dropMenu.goSelected).SingleOrDefault();
                GameObject selectedGoInstance = Instantiate(selectedGo, new Vector3(hit.point.x, hit.point.y + selectedGo.GetComponent<MeshRenderer>().bounds.size.y / 2, hit.point.z), Quaternion.identity);
                selectedGoInstance.transform.up = hit.normal;
                ObjectsData m = new ObjectsData(selectedGo.name, selectedGoInstance.transform.position, selectedGoInstance.transform.localScale, selectedGoInstance.transform.rotation);         
                c.objectsList.Add(m);
            }
        }   
    }

    [CustomEditor(typeof(PlayerController))]
    public class ObjectBuilderEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            
            PlayerController myScript = (PlayerController)target;
            if(GUILayout.Button("Create objects"))
            {
                string path = Application.dataPath + "/" + "Data.json";
                string jsonString = File.ReadAllText (path); 
                Container listaRecords = new Container();
                ObjectsData[] t = JsonHelper.getJsonArray<ObjectsData>(jsonString);
                listaRecords.objectsList = t.ToList();
                foreach(ObjectsData c in listaRecords.objectsList)
                {
                    string n = c.name.Split('(')[0];
                    Debug.Log(n);
                    GameObject g = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/"+n+".prefab", typeof(GameObject)) as GameObject; 
                    Debug.Log(g);
                    Debug.Log(Application.dataPath+ "/Prefabs/"+n+".prefab");
                    GameObject u = Instantiate(g, c.position, c.rotation);
                    u.tag = "New";
                }
            }
             if(GUILayout.Button("Delete objects"))
            {
                for (int i = 0; i < GameObject.FindGameObjectsWithTag("New").Length; i++)
                {
                    DestroyImmediate(GameObject.FindGameObjectsWithTag("New")[i]);
                }
            }
        }
    }
    void OnApplicationQuit()
    {
        string data =JsonHelper.ToJson(c.objectsList.ToArray());
        Debug.Log(data);
        File.WriteAllText(Application.dataPath + "/" + "Data.json", data.ToString());
        Debug.Log(Application.dataPath + "/" + "Data.json");
    }

}
