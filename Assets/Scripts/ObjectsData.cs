﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ObjectsData
{
    public string name;
    public Vector3 position;
    public Vector3 scale;
    public Quaternion rotation;
    public ObjectsData(string _name, Vector3 _pos, Vector3 _scale, Quaternion _rot)
    {
        name = _name;
        position = _pos;
        scale = _scale;
        rotation = _rot;     
    }
}
