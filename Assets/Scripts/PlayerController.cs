﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public List<GameObject> go;

    Rigidbody rb;
    float speed = 2;
    void Start()
    {
         rb = GetComponent<Rigidbody> ();
    }
   

    // Update is called once per frame
    Vector2 rotation = new Vector2(0,0);
    void Update()
    {
        if(gameObject!=null &&  gameObject.GetComponent(typeof(CharacterController)) )
        {
 
            CharacterController cc = (CharacterController)gameObject.GetComponent( typeof(CharacterController));
            rotation.y += Input.GetAxis ("Mouse X");
		    rotation.x += -Input.GetAxis ("Mouse Y");
		    transform.eulerAngles = (Vector2)rotation * speed;
            // float rotateSpeed = 10.0f;
            // float rotationX = Input.GetAxis ("Mouse Y") * rotateSpeed;
            // float rotationY = Input.GetAxis ("Mouse X") * rotateSpeed;
            // transform.Rotate (0 , rotationY , rotationX);
           
            float moveSpeed = 40.0f;
            float dt = Time.deltaTime;
            float dy =  0;
            if(Input.GetKey(KeyCode.Space))
            {
                dy = moveSpeed * dt;
            }
            if(Input.GetKey(KeyCode.P))
            {
                dy -= moveSpeed * dt;
            }
            float dx = Input.GetAxis("Horizontal") * dt * moveSpeed;
            float dz= Input.GetAxis("Vertical") * dt * moveSpeed;
           
            cc.Move(transform.TransformDirection(new Vector3(dx, dy,dz))  );
        }
    }
}

